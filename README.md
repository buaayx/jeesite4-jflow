#### 项目介绍 jeesite4-jflow 

1. jeesite4-JFlow 是jeesite开发平台与jflow流程开发平台集成的版本.
2. 您即可以使用jeesite的敏捷性开发，也可以使用JFlow的流程引擎，表单引擎的功能. 
3. 我们已经把jeesite与jflow的组织结构集成在一起,流程开发平台与敏捷开发平台可以无缝的嫁接在一起. 两者珠联璧合,是您开发的利器,不二的选择.
4. jeesite 官方网站: http://jeesite.com  驰骋BPM的官方网站 http://ccflow.org.
5. 技术交流群: 819789189 , 商务合作电话: 黄老师 13864102942
6. 在线演示地址: http://140.143.236.168:8980/js 用户名system 密码 admin 


#### 软件架构
1. jeesite4.0 敏捷开发平台.
2. 驰骋bpm, 流程开发平台.

#### 安装教程

1. 环境准备：`JDK 1.8`、`Maven 3.3`、`MySQL 5.7`
2. 下载源码：<https://gitee.com/thinkgem/jeesite4-jflow>
3. 数据库安装：https://gitee.com/thinkgem/jeesite4-jflow/attach_files 下载数据库脚本文件,并安装数据库.
4. 打开文件： /web/src/main/resources/config/jeesite.yml` 配置JDBC连接
5. 执行脚本： /web/bin/run-tomcat.bat` 启动服务即可
6. 浏览器访问：<http://127.0.0.1:8980/js/>  账号 system 密码 admin
7. 常见问题：<http://jeesite4.mydoc.io/?t=284210>

#### 使用说明

1. JFlow官网：<http://ccflow.org>
2. JFlow源码：<https://gitee.com/opencc/JFlow>
3. JFlow帮助文档：<http://ccbpm.mydoc.io>
4. JFlow视频教程下载地址 SVN地址：http://140.143.236.168:7080/svn/ccbpmdocs 用户名: ccbpm 密码:ccbpm
5. CCForm表单引擎帮助文档：<http://ccform.mydoc.io>

#### 其他连接.

1. jeesite3.0 https://gitee.com/thinkgem/jeesite
2. jeesite4.0 https://gitee.com/thinkgem/jeesite4
3. jflow https://gitee.com/opencc/JFlow
4. asp.net版本的ccflow https://gitee.com/opencc/ccflow

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
 